//1.

db.users.find(
	{
		$or: [
			{
				"firstName" : { $regex: "A", $options: "i"}
			},
			{
				"lastName" : { $regex: "A", $options: "i"}
			},
		]
	},
	{
		"firstName": 1,
		"lastName": 1,
		"_id": 0
	}
)

//2.
db.users.find(
	{
		$and:[
			{
				"isAdmin" : true
			},
			{
				"isActive": true
			}
		]	
	}
)

//3.

db.courses.find(
	{
		"Name": {$regex: "u", $options: "i"},
		"Price": {$gte: 13000}		
	}
)